#include "spimcore.h"

#define DEBUG 0

/* 10 Points */
// -----[ ALU ]-----------------------------------------------------------------
// Description     Choose the operation with the int type casted ALUControl and
//                 preform the operation. If the result is zero set Zero to 1
//                 otherwise set Zero to 0.
void ALU(unsigned A,unsigned B,char ALUControl,unsigned *ALUresult,char *Zero) {

    // Switch to do appropriate ALU operation
    switch ((int)ALUControl) {

        // Add
        case 0:

            if(DEBUG) printf("  -> add[i]: %u + %u = %u\n", A, B, A + B);

            *ALUresult = A + B;
            break;

        // Sub
        case 1:

            if(DEBUG) printf("  -> sub[i]: %u - %u = %u\n", A, B, A - B);

            *ALUresult = A - B;
            break;

        // Set on Less Than Signed
        case 2:

            if(DEBUG) printf("  -> slt[i]: %u < %u = %d\n", (signed)A,
                (signed)B, ((signed)A < (signed)B) ? 1 : 0);

            *ALUresult = ((signed)A < (signed)B) ? 1 : 0;
            break;

        // Set on Less Than Unsigned
        case 3:

            if(DEBUG) printf("  -> slt[i]: %u < %u = %d\n", A, B,
                (A < B) ? 1 : 0);

            *ALUresult = (A < B) ? 1 : 0;
            break;

        // And
        case 4:

            if(DEBUG) printf("  -> and[i]: %u & %u = %u\n", A, B, A & B);

            *ALUresult = A & B;
            break;

        // or
        case 5:

            if(DEBUG) printf("  -> or[i]: %u | %u = %u\n", A, B, A | B);

            *ALUresult = A | B;
            break;

        // Load Word Upper
        case 6:

            if(DEBUG) printf("  -> lwu[i]\n");

            *ALUresult = B << 16;
            break;

        // Not
        case 7:

            if(DEBUG) printf("  -> not[i]\n");

            *ALUresult = !A;
            break;

        default: break;


    }

    // Set the zero flag to 1 if the result is zero
    *Zero = (*ALUresult == 0) ? 1 : 0;

    if(DEBUG) printf("  -> Zero: %s\n\n", (*Zero) ? "true" : "false");
}

/* 10 Points */
// -----[ instruction_fetch ]---------------------------------------------------
// Description     Get the instruction from memory that is stored in (PC * 4)
//                 Check to see if the memory is word aligned
//
// Return          1 if Hault 0 otherwise
int instruction_fetch(unsigned PC,unsigned *Mem,unsigned *instruction) {

    if(DEBUG) printf("  -> PC: %u %% 4 = %u\n", PC, PC % 4);

    // Check PC is word aligned
    if(PC % 4 == 0) {

        // Retreve instuction from memory at PC * 4
        *instruction = Mem[PC >> 2];

        if(DEBUG) printf("  -> instruction: %08x\n\n", *instruction);

        return 0;

    }

    // Hold Condition
    return 1;
}

/* 10 Points */
// -----[ instruction_partition ]-----------------------------------------------
// Description     Use right shifts and logical and to partition the instruction
//                 into op, r1, r2, r3, funct, offset, and jsec.
//                 0x3f -> Takes last 6 bits of binary representation
//                         0011 1111
//                 0x1f -> Takes last 5 bits of binary representation
//                         0001 1111
//                 0xffff -> Takes last 16 bits of binary representation
//                         1111 1111 1111 1111
//                 0x3ffffff -> Takes last 26s bits of binary representation
//                         0011 1111 1111 1111 1111 1111 1111
void instruction_partition(unsigned instruction, unsigned *op, unsigned *r1,
    unsigned *r2, unsigned *r3, unsigned *funct, unsigned *offset,
    unsigned *jsec) {

    // Partition instructions with Shifts and Logical and
    *op = (instruction >> 26) & 0x3f;
    *r1 = (instruction >> 21) & 0x1f;
    *r2 = (instruction >> 16) & 0x1f;
    *r3 = (instruction >> 11) & 0x1f;
    *funct = instruction & 0x3f;
    *offset = instruction & 0xffff;
    *jsec = instruction & 0x3ffffff;

    if(DEBUG) {
        printf("  -> +----------hex----------+\n");
        printf("  -> | op = %-17x|\n", *op);
        printf("  -> | r1 = %-17x|\n", *r1);
        printf("  -> | r2 = %-17x|\n", *r2);
        printf("  -> | r3 = %-17x|\n", *r3);
        printf("  -> | funct = %-14x|\n", *funct);
        printf("  -> | offset = %-13x|\n", *offset);
        printf("  -> | jsec = %-15x|\n", *jsec);
        printf("  -> +-----------------------+\n\n");
    }
}

// -----[ setControls ]---------------------------------------------------------
// Description     Initalize the control signals with the passed in values
void setControls(int RegDst, int Jump, int Branch, int MemRead, int MemtoReg,
    int ALUOp, int MemWrite, int ALUSrc, int RegWrite,
    struct_controls *controls) {

    // Set Control Signals in Controls Struct
    controls->RegDst = RegDst; 
    controls->Jump = Jump; 
    controls->Branch = Branch; 
    controls->MemRead = MemRead;
    controls->MemtoReg = MemtoReg;
    controls->ALUOp = ALUOp;
    controls->MemWrite = MemWrite; 
    controls->ALUSrc = ALUSrc;
    controls->RegWrite = RegWrite;

    if(DEBUG) {
        printf("  -> +---------Decimal-------+\n");
        printf("  -> | RegDst = %-13u|\n", controls->RegDst);
        printf("  -> | Jump = %-15u|\n", controls->Jump);
        printf("  -> | Branch = %-13u|\n", controls->Branch);
        printf("  -> | MemRead = %-12u|\n", controls->MemRead);
        printf("  -> | MemtoReg = %-11u|\n", controls->MemtoReg);
        printf("  -> | ALUOp = %-14u|\n", controls->ALUOp);
        printf("  -> | MemWrite = %-11u|\n", controls->MemWrite);
        printf("  -> | ALUSrc = %-13u|\n", controls->ALUSrc);
        printf("  -> | RegWrite = %-11u|\n", controls->RegWrite);
        printf("  -> +-----------------------+\n\n");
    }
}

/* 15 Points */
// -----[ instruction_decode ]--------------------------------------------------
// Description     Uses op code to set the control signals by passing the
//                 desired signals to setControls
//
// Return          1 if Hault 0 otherwise
int instruction_decode(unsigned op,struct_controls *controls) {

    // DEBUG see setControls Debug

    switch(op) {

        // R Type
        case 0: setControls(1, 0, 0, 0, 0, 7, 0, 0, 1, controls);
            break;

        // Jump
        case 2: setControls(0, 1, 0, 0, 0, 0, 0, 0, 0, controls);
            break;

        // Branch on Equal
        case 4: setControls(1, 0, 1, 0, 2, 1, 0, 0, 0, controls);
            break;

        // Add Immediate
        case 8: setControls(0, 0, 0, 0, 0, 0, 0, 1, 1, controls);
            break;

        // Set on Less Than Immediate
        case 10: setControls(0, 0, 0, 0, 0, 2, 0, 1, 1, controls);
            break;

        // Set on Less Than Immediate Unsigned
        case 11: setControls(0, 0, 0, 0, 0, 3, 0, 1, 1, controls);
            break;

        // 001111 Load Word Unsigned Imediate
        case 15: setControls(0, 0, 0, 0, 0, 6, 0, 1, 1, controls);
            break;

        // 100011 Load Word
        case 35: setControls(0, 0, 0, 1, 1, 0, 0, 1, 1, controls);
            break;

        // 101011 Store Word
        case 43: setControls(2, 0, 0, 0, 2, 0, 1, 1, 0, controls);
            break;

        // Hault
        default: return 1;
    }

    return 0;
}

/* 5 Points */
// -----[ read_register ]-------------------------------------------------------
// Description     Retreve the data that is store in the two designated
//                 registers
void read_register(unsigned r1,unsigned r2,unsigned *Reg,unsigned *data1,
    unsigned *data2) {

    // DEBUG None

    *data1 = Reg[r1];
    *data2 = Reg[r2];
}


/* Sign Extend */
/* 10 Points */
// -----[ sign_extend ]---------------------------------------------------------
// Description     If the left most bit of the 16 bit offset is 1 use logical or
//                 to add 16 1's to the begining (Binary)
//                 Otherwise add 16 0's to the begining (Binary)
//                 0xffff0000 -> 1111 1111 1111 1111 0000 0000 0000 0000
//                 0x0000ffff -> 0000 0000 0000 0000 1111 1111 1111 1111
void sign_extend(unsigned offset,unsigned *extended_value) {

    // DEBUG None

    // Negative Entended
    if((offset >> 15) == 1)
        *extended_value = offset | 0xffff0000;

    // Positive Extended
    else
        *extended_value = offset & 0x0000ffff;
}

/* 10 Points */
// -----[ ALU_operations ]------------------------------------------------------
// Description     If the ALUop is 7 then the instruction was an R type. If this
//                 is the case then use the function code to change the ALUop
//                 code. If ALUsrc is 1 then use the extended value from I type
//                 instruction as data2. Call ALU with necessary parameters.
//
// Return          1 if Hault 0 otherwise
int ALU_operations(unsigned data1,unsigned data2,unsigned extended_value,
    unsigned funct,char ALUOp,char ALUSrc,unsigned *ALUresult,char *Zero) {

    // DEBUG see setContols Debug

    if(ALUOp == 7) {

        switch(funct) {

            // Shift Right Logical Variable
            case 6: ALUOp = 6;
                break;

            // Add
            case 32: ALUOp = 0;
                break;

            // Sub
            case 34: ALUOp = 1;
                break;

            // And
            case 36: ALUOp = 4;
                break;

            // Or
            case 37: ALUOp = 5;
                break;

            // Nor
            case 39: ALUOp = 7;
                break;

            // Set on Less Than
            case 42: ALUOp = 2;
                break;

            // Set on Less Than Unsigned
            case 43: ALUOp = 3;
                break;

            // Halt
            default:
                return 1;
        }
    }

    if(ALUSrc == 1)
        data2 = extended_value;

    ALU(data1,data2,ALUOp,ALUresult,Zero);

    return 0;
}

/* 10 Points */
// -----[ rw_memory ]-----------------------------------------------------------
// Description     If MemRead is asserted and ALUresult(Address) is word aligned
//                 then store the data in Mem[ALUresult * 4] in memdata.
//                 If MemWrite is asserted and ALUresult(Address) is word
//                 aligned then store the data in data2 in Mem[ALUresult * 2].
//
// Return          1 if Hault 0 otherwise
int rw_memory(unsigned ALUresult,unsigned data2,char MemWrite,char MemRead,
    unsigned *memdata,unsigned *Mem) {

    // DEBUG None

    // Read Memory
    if (MemRead == 1) {

        if((ALUresult % 4) == 0)
            *memdata = Mem[ALUresult >> 2];
        // Hault
        else
            return 1;
    }

    // Write Memory
    if (MemWrite == 1) {

        if((ALUresult % 4) == 0)
            Mem[ALUresult >> 2] = data2;
        // Hault
        else
            return 1;
    }

    return 0;
}

/* 10 Points */
// -----[ write_register ]------------------------------------------------------
// Description     :
void write_register(unsigned r2,unsigned r3,unsigned memdata,unsigned ALUresult,
    char RegWrite,char RegDst,char MemtoReg,unsigned *Reg) {

    // DEBUG None

    if(RegWrite == 1) {

        // rt
        if(RegDst == 0)
            Reg[r2] = (MemtoReg == 1) ? memdata : ALUresult;
        // rd
        else
            Reg[r3] = (MemtoReg == 1) ? memdata : ALUresult;
    }
}

/* 10 Points */
// -----[ PC_update ]-----------------------------------------------------------
// Description     :
void PC_update(unsigned jsec,unsigned extended_value,char Branch,char Jump,
    char Zero,unsigned *PC) {

    // DEBUG see Instuction Fetch Debug

    *PC += 4;

    if(Branch && Zero)
        *PC += extended_value << 2;
    else if(Jump)
        *PC = (jsec << 2) | (*PC & 0xf0000000);
}